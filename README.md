# 信息收集系统
在大学里，辅导员经常委托班长、团支书收集一些班里同学的信息，而传统的方式比较繁琐（辅导员——execl——qq/微信——班长——同学——Excel——班长（工作大头：汇总n个Execl）——Excel——辅导员）太烦了，毕设就自己选个题目做这个。

本项目为本人的毕设项目，主要为了学习一下spring boot搭建web工程、学习其他点的Demo。[点击这里查看 Demo](http://itlym.cn:8080)

### 演示：
[点击这里查看 Demo](http://itlym.cn:8080)
演示账号 用户名:123456
演示账号 密码:123456

（不支持QQ/微信登录）

### 可以帮助java入门同学学习一点web相关的

##### 依赖的框架
- Spring Boot   整合
- Spring 		松耦合
- Spring MVC	web 请求流程
- Spring Data jpa	数据持久层
- Thymeleaf		模板引擎（类似 jsp，前后分离可另选型）
- log4j			日志框架

### web开发常用
数据库的设计
注解的开发和使用、AOP相关
web常见安全应对（sql注入、css攻击、xss攻击）
加解密【对称加密(AES)、非对称加密(ECSA、RSA)、密钥的分级管理、分布式密钥管理、安全摘要与Hash(SHA256)、传输加密(ECDHE)】
异步队列
第三方登录
第三方分享

---

### 部署
- java 8+	    推荐 11+				
- maven 3+	[官网最新地址](http://maven.apache.org/download.cgi)
- mysql 5.5+	（数据库版本过高时需要处理 ssh）[官网5.5地址](https://dev.mysql.com/downloads/mysql/5.5.html)

### 运行依赖
#### 必须：
- 依赖的JS css 等在src/main/resources/static.rar，需要解压到项目内src/main/resources
- 数据库表结构在 msg_collect.sql，不包含表数据，需要导入数据库中，并修改 src/main/resources/application.yml，将数据库密码等连接参数改为实际数据库
#### 可选：
- msg_collect_files.rar 包含基本的头像，需要解压至任意路径，并修改  src/main/java/com/msgc/config/WebMvcConfig.java 的 FILE_DIR 变量，若无此目录会自动创建
- 日志文件保存目录为 log4j.properties 中的目录，若无此目录会自动创建

## 运行时需要先注册，无内置账号

## 需要修改
- 项目的静态路径 WebMvcConfig.java(com/msgc/config)
- 数据库连接	application.yml
- 日志输出路径 log4j.properties


### 其他：
    MQSQL设置时区,出现时区乱码问题则使用 SET GLOBAL time_zone='+8:00';
