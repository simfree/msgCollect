package com.msgc.utils.crypto.negotiation.exception;

import com.msgc.utils.crypto.exception.CryptoException;

/**
 *	传输加解密出错 ECDH
 * @author liuyanming 2019年12月18日
 */
public class TransportCryptoException extends CryptoException {

	public TransportCryptoException(String message, Throwable cause) {
		super(message, cause);
	}
	
}
