package com.msgc.utils.crypto.negotiation.service;

import com.msgc.utils.crypto.negotiation.cache.dto.KeyExchangeResult;
import com.msgc.utils.crypto.negotiation.dto.KeyExchangeRequest;
import com.msgc.utils.crypto.negotiation.dto.KeyExchangeResponse;
import com.msgc.utils.crypto.negotiation.exception.NegotiationException;

/**
 * @author Admin
 */
public interface TransportNegotiationService {

    /**
     * 与服务方进行密钥协商请求
     * @param serverUrl 目标服务的交换地址
     * @return 是否协商成功
     */
    KeyExchangeResult requestForNegotiate(String serverUrl) throws NegotiationException;


    /**
     * 处理其他服务发起的密钥交换请求
     * @param keyExchangeRequest 请求参数
     * @return 是否协商成功
     */
    KeyExchangeResponse handleNegotiate(KeyExchangeRequest keyExchangeRequest) throws NegotiationException;

}
