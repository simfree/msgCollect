package com.msgc.utils.crypto.negotiation.exception;

import com.msgc.utils.crypto.exception.CryptoException;

/**
 * 密钥协商出错
 * @author liuyanming 2019年12月18日
 */
public class NegotiationException extends CryptoException {

	public NegotiationException(String message, Throwable cause) {
		super(message, cause);
	}

	public NegotiationException(String message) {
		super(message);
	}
}
