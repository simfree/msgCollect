package com.msgc.utils.crypto.negotiation.interceptor;

import com.msgc.utils.crypto.negotiation.cache.KeyNegotiationCache;
import com.msgc.utils.crypto.negotiation.cache.TransportCipherHolder;
import com.msgc.utils.crypto.negotiation.cache.cipher.TransportCipher;
import com.msgc.utils.crypto.negotiation.cache.dto.KeyExchangeResult;
import com.msgc.utils.crypto.negotiation.constant.KeyExchangeConstants;
import com.msgc.utils.crypto.negotiation.util.TransportCryptoUtilAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 安全传输拦截器
 * 只拦截握手完毕后的加密接口，即只拦截header中带 xSessionId 和 xDk 的请求。
 *
 * @author Admin
 */
@Component
public class ExchangeKeyInterceptor extends HandlerInterceptorAdapter {

    private static final Logger log = LoggerFactory.getLogger(ExchangeKeyInterceptor.class);

    private KeyNegotiationCache keyNegotiationCache;

    private TransportCryptoUtilAdapter transportCryptoUtil;

    public ExchangeKeyInterceptor(KeyNegotiationCache keyNegotiationCache, TransportCryptoUtilAdapter transportCryptoUtil) {
        this.keyNegotiationCache = keyNegotiationCache;
        this.transportCryptoUtil = transportCryptoUtil;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String token = request.getHeader(KeyExchangeConstants.TOKEN);
        String xSessionId = request.getHeader(KeyExchangeConstants.SECURITY_SESSION_ID);
        String xDk = request.getHeader(KeyExchangeConstants.SECURITY_DATA_KEY);
        if (StringUtils.isEmpty(xSessionId) || StringUtils.isEmpty(xDk) || StringUtils.isEmpty(token)) {
            return true;
        }

        // 一、处理请求：解密发送方的密钥
        KeyExchangeResult cacheKeyExchangeResult = keyNegotiationCache.getAsServer(xSessionId);
        //todo
        assert cacheKeyExchangeResult != null;

        transportCryptoUtil.verifyToken(xSessionId, xDk, token);

        log.debug("security request. xDk is " + xDk);
        // 解密数据密钥
        byte[] requestDk = TransportCryptoUtilAdapter.decryptDk(cacheKeyExchangeResult, xDk);
        // 请求解密处理器
        TransportCipher requestDecrypt = new TransportCipher(cacheKeyExchangeResult, requestDk);
        TransportCipherHolder.setRequestDecryptHandler(requestDecrypt);

        // 二、预生成返回值加密的数据密钥，以便于加密要返回的敏感数据信息（请求和响应中使用的数据密钥不同）
        byte[] responseDk = TransportCryptoUtilAdapter.generateDataKey(cacheKeyExchangeResult.getKeyLength());
        // 响应加密处理器
        TransportCipher responseEncrypt = new TransportCipher(cacheKeyExchangeResult, responseDk);
        TransportCipherHolder.setResponseCryptHandler(responseEncrypt);
        String responseX_Dk = TransportCryptoUtilAdapter.encryptDk(cacheKeyExchangeResult, responseDk);
        log.info("security response. xDk is " + responseX_Dk);
        response.setHeader("Token", transportCryptoUtil.generateToken(xSessionId, responseX_Dk));
        response.setHeader("xSessionId", cacheKeyExchangeResult.getxSessionId());
        response.setHeader("xDk", responseX_Dk);
        response.setContentType(MediaType.APPLICATION_JSON_UTF8_VALUE);
        return true;
    }

}
