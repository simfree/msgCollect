package com.msgc.utils.crypto.negotiation.cache.cipher;

import com.msgc.utils.crypto.aes.exception.AesCryptoException;
import com.msgc.utils.crypto.negotiation.cache.dto.KeyExchangeResult;
import com.msgc.utils.crypto.negotiation.util.TransportCryptoUtilAdapter;

/**
 * 传输加解密，使用者时
 * @author Admin
 */
public class TransportCipher {

    private KeyExchangeResult keyExchangeResult;

    /** 数据密钥明文 */
    private byte[] dk;

    public TransportCipher(KeyExchangeResult keyExchangeInfo, byte[] dk) {
        this.keyExchangeResult = keyExchangeInfo;
        this.dk = dk;
    }

    /**
     * 加密
     */
    public String encrypt(String toCipher) throws AesCryptoException {
        return TransportCryptoUtilAdapter.encrypt(keyExchangeResult, dk, toCipher);
    }

    /**
     * 解密
     */
    public String decrypt(String cipherText) throws AesCryptoException {
        return TransportCryptoUtilAdapter.decrypt(keyExchangeResult, dk, cipherText);
    }

}
