package com.msgc.utils.crypto.negotiation.cache;

import com.msgc.utils.crypto.negotiation.cache.cipher.TransportCipher;

/**
 * 保存线程变量 —— 传输加解密处理器
 *
 * @author Admin
 */
public class TransportCipherHolder {

    /**
     * 发起请求 和 处理请求时，使用这个
     */
    private static ThreadLocal<TransportCipher> request = new ThreadLocal<>();

    /**
     * 接收响应 和 响应对方时，用这个
     */
    private static ThreadLocal<TransportCipher> response = new ThreadLocal<>();

    public static TransportCipher getClientHandler() {
        return request.get();
    }

    public static TransportCipher getServerHandler() {
        return response.get();
    }

    /**
     * 用于解密收到的请求 or 加密发送请求
     */
    public static void setRequestDecryptHandler(TransportCipher transportCipher) {
        request.set(transportCipher);
    }

    /**
     * 用于加密响应 or 解密对方的响应
     */
    public static void setResponseCryptHandler(TransportCipher transportCipher) {
        response.set(transportCipher);
    }

    public static void cleanRequestDecryptHandler() {
        request.remove();
    }

    public static void cleanResponseCryptHandler() {
        response.remove();
    }


}
