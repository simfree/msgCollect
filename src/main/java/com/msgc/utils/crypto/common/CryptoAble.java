package com.msgc.utils.crypto.common;

import com.msgc.utils.crypto.exception.CryptoException;

import java.security.GeneralSecurityException;

/**
 * 加解密能力
 * @author liuyanming 2019年9月29日23:30:58
 */
public interface CryptoAble extends ByteSpecification {

    /**
     * 加密
     * @param text 明文
     * @return  加密后的密文
     */
    String encrypt(String text) throws CryptoException;

    /**
     *  解密
     * @param cipher    密文
     * @return          解密后的明文
     */
    String decrypt(String cipher) throws CryptoException;

}
