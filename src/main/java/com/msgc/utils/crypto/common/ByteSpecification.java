package com.msgc.utils.crypto.common;

import org.apache.commons.codec.Charsets;

import java.nio.charset.Charset;
import java.util.Base64;

/**
 * 加密编码规范
 * @author Admin
 */
public interface ByteSpecification {
    /** 统一字符编码 */
    Charset CHARSET_UTF_8 = Charsets.UTF_8;

    /** 统一比特编码 */
    static String encodeToString(byte[] bytes){
        return new String(Base64.getEncoder().encode(bytes), CHARSET_UTF_8);
    }

    /** 统一比特解码 */
    static byte[] decodeToBytes(String base64String){
        return Base64.getDecoder().decode(base64String);
    }
}
