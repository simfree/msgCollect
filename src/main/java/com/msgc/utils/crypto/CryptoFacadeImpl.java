package com.msgc.utils.crypto;

import com.msgc.utils.crypto.aes.AesCryptoAble;
import com.msgc.utils.crypto.aes.exception.SymmetricCryptoException;
import com.msgc.utils.crypto.asymmetric.AsymmetricCryptoService;
import com.msgc.utils.crypto.asymmetric.exception.AsymmetricCryptoException;
import com.msgc.utils.crypto.asymmetric.exception.KeyPairException;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 加解密工具
 * @author liuyanming 2019年9月26日14:21:02
 */
@Service
public class CryptoFacadeImpl implements CryptoFacade {

    private final AesCryptoAble aes;

    private final AsymmetricCryptoService rsa;

    @Autowired
    public CryptoFacadeImpl(AesCryptoAble aes, AsymmetricCryptoService rsa) {
        this.aes = aes;
        this.rsa = rsa;
    }

    @Override
    public String encryptAes(@NonNull String text) throws SymmetricCryptoException {
        return aes.encrypt(text);
    }

    @Override
    public String decryptAes(@NonNull String cipherText) throws SymmetricCryptoException {
        return aes.encrypt(cipherText);
    }

    @Override
    public void initAes() {
        aes.ensureEncryption();
    }

    // ================================ 传输加解密（如前后交互） =====================================

    @Override
    public String getRsaPk() throws KeyPairException {
        return rsa.getPublicKey();
    }

    @Override
    public String encryptRsa(String text) throws AsymmetricCryptoException {
        return rsa.encrypt(text);
    }
    
    @Override
    public String decryptRsa(String cipherText) throws AsymmetricCryptoException {
        return rsa.decrypt(cipherText);
    }

    @Override
    public String encryptRsa(String text, String publicKey) throws AsymmetricCryptoException {
        return rsa.encrypt(text, publicKey);
    }

    @Override
    public String signRsa(String text) throws AsymmetricCryptoException {
        return rsa.sign(text);
    }

    @Override
    public boolean verifyRsa(String text, String signature) throws AsymmetricCryptoException {
        return rsa.verify(text, signature);
    }

}
