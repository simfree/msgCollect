package com.msgc.utils.crypto.util;


import com.msgc.utils.WebUtil;
import com.msgc.utils.crypto.CryptoFacade;
import com.msgc.utils.crypto.aes.exception.SymmetricCryptoException;
import com.msgc.utils.crypto.asymmetric.exception.AsymmetricCryptoException;
import com.msgc.utils.crypto.asymmetric.exception.KeyPairException;

/**
 * 加解密工具门面
 *
 * @author liuyanming 2019年9月26日14:25:11
 * @since 1.4
 */
public final class CryptoUtil {

    private static CryptoFacade crypto;

    static {
        crypto = WebUtil.getBean(CryptoFacade.class);
        if(crypto == null){
            throw new IllegalStateException("Can't use CryptoUtil now! Maybe application context was not initialized.");
        }
    }

    // ================================ 存储加解密 =====================================

    /**
     * 本地存储加密
     * @param text	待加密数据，不能为null，否则 NPE
     * @return		参数 text 加密后的密文
     * @throws SymmetricCryptoException 加密异常
     */
    public static String encryptAes(String text) throws SymmetricCryptoException {
        return crypto.encryptAes(text);
    }

    /**
     * 本地存储加密解密
     * @param cipherText	密文，不能为null，否则 NPE
     * @return				参数 cipherText 解密后的明文
     * @throws SymmetricCryptoException 加密异常
     */
    public static String decryptAes(String cipherText) throws SymmetricCryptoException {
        return crypto.decryptAes(cipherText);
    }

    /**
     * 确保本地加密功能正常使用
     * 推荐初始化时调用，可优化第一次加解密性能。
     */
    public static void initAes() {
        crypto.initAes();
    }

    // ================================ 传输加解密（如前后交互） =====================================

    /**
     * 获取 RSA 公钥
     * @return 公钥
     */
    public static String getPk() throws KeyPairException {
        return crypto.getRsaPk();
    }

    /**
     * 加密
     * @param text                待加密数据
     * @return 加密后的
     * @throws AsymmetricCryptoException RsaCryptoException
     */
    public static String encryptRsa(String text) throws AsymmetricCryptoException {
        return crypto.encryptRsa(text);
    }

    /**
     * 解密
     * @param cipherText   	待解密的数据，密文
     * @return 解密后的
     * @throws AsymmetricCryptoException RsaCryptoException
     */
    public static String decryptRsa(String cipherText) throws AsymmetricCryptoException {
        return crypto.decryptRsa(cipherText);
    }


    /**
     * RSA加密（公钥加密）
     * @param text 需加密的数据
     * @param publicKey 对方的公钥
     * @return 密文
     * @throws AsymmetricCryptoException 加解密出错
     */
    public String encryptRsa(String text, String publicKey) throws AsymmetricCryptoException {
        return crypto.encryptRsa(text, publicKey);
    }

    /**
     * 签名
     * @param text 签名内容
     * @return 签名结果
     * @throws AsymmetricCryptoException 加解密出错
     */
    public String signRsa(String text) throws AsymmetricCryptoException {
        return crypto.signRsa(text);
    }

    /**
     * 签名验证
     * @param text   内容
     * @param signature 签名
     * @return 是否合法
     * @throws AsymmetricCryptoException 加解密出错
     */
    public boolean verifyRsa(String text, String signature) throws AsymmetricCryptoException {
        return crypto.verifyRsa(text, signature);
    }

}
