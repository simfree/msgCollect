package com.msgc.utils.crypto.hash;

/**
 * 信息摘要能力接口
 * @author Admin
 */
public interface DigestAble {

    /**
     * 离散
     * @param bytes 待离散的数组
     * @return  摘要
     */
    byte[] hash(byte[] bytes);
}
