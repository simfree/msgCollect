package com.msgc.utils.crypto.aes.dao;

import com.msgc.utils.crypto.aes.entity.AesInfoEntity;

import javax.validation.constraints.NotNull;

/**
 * AES 加密所需信息持久化接口
 * 	实现类保证持久化即可，不限制实现方式
 * @author liuyanming 2019年9月29日23:24:40
 */
public interface IAesInfoDao {
	
	/**
	 * 保存加密信息
	 * @param aesInfo  待保存的的加密信息
	 * @throws Exception when aesInfo persist fail.
	 */
	void save(@NotNull AesInfoEntity aesInfo) throws Exception;
	
	/**
	 * 获取加密信息
	 * @return AesInfoEntity
	 */
	AesInfoEntity get() throws Exception;

}
