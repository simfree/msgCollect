package com.msgc.utils.crypto.aes.exception;

import com.msgc.utils.crypto.exception.CryptoException;

/**
 * 对称加解密出错
 * @author liuyanming 2019年12月18日
 */
public class SymmetricCryptoException extends CryptoException {

	public SymmetricCryptoException(String message, Throwable cause) {
		super(message, cause);
	}
	
}
