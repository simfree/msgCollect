package com.msgc.utils.crypto.aes.dao.mapper;


/*
@Mapper
public interface SecurityMapper {
	
	*/
/**
	 * 插入加密固件表
	 * @param security       加密信息
	 *//*

	@Insert("insert into tb_security(tid, component, data_type, s_data_key, s_random_digit, s_encrypt_decode, create_time, update_time)"
			+ "values (#{security.tid}, #{security.component}, #{security.dataType}, #{security.dataKey}, #{security.randomDigit}, #{security.ivParameter}, #{security.createTime}, #{security.updateTime}) ")
	void insertSecurity(@Param("security") SecurityEntity security);
	
	*/
/**
	 * 查询设备密码
	 * @param component       组件标识
	 * @param type            类型
	 * @return
	 *//*

	@Results({
		@Result(property="tid", column="tid", id=true), 
		@Result(property="dataKey", column="s_data_key"), 
		@Result(property="randomDigit", column="s_random_digit"), 
		@Result(property="ivParameter", column="s_encrypt_decode"), 
		@Result(property="component", column="component"), 
		@Result(property="dataType", column="data_type")
	})
	@Select("select * from tb_security where component = #{component} and data_type = #{type}")
	SecurityEntity findSecurityByComponent(@Param("component") String component, @Param("type") Integer type);
}
*/
