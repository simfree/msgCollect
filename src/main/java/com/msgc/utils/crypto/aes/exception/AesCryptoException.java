package com.msgc.utils.crypto.aes.exception;

/**
 * AES 加解密出错
 * @author liuyanming 2019年12月18日
 */
public class AesCryptoException extends SymmetricCryptoException {

	public AesCryptoException(String message, Throwable cause) {
		super(message, cause);
	}
	
}
