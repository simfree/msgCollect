package com.msgc.utils.crypto.aes.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * AES加解密相关秘钥存储
 * @author liuyanming 2019年9月29日23:22:07
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AesInfoEntity {
    /** 主键 */
    String id;
    /** 应用唯一标识 */
    String appId;
    /** 数据密钥 */
    String dataKey;
    /** 跟密钥部件，用于生成根密钥 */
    String rootKeyPart;
    /** 加密向量 */
    String iv;
    /** 创建时间 */
    Date createTime;
    /** 版本信息 */
    String version;
}
