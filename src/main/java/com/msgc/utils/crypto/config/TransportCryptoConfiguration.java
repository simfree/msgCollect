package com.msgc.utils.crypto.config;

import com.msgc.utils.crypto.negotiation.cache.KeyNegotiationCache;
import com.msgc.utils.crypto.negotiation.cache.LocalKeyNegotiationCache;
import com.msgc.utils.crypto.negotiation.cache.RedisKeyNegotiationCache;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.client.RestTemplate;

/**
 * 传输加密缓存配置
 * @author Admin
 */
@Slf4j
@Configuration
public class TransportCryptoConfiguration {

    @Bean("redisKeyNegotiationCache")
    @ConditionalOnProperty(name = "msgc.cluster", havingValue = "true")
    @ConditionalOnBean(RedisTemplate.class)
    @ConditionalOnMissingBean(value = {KeyNegotiationCache.class})
    public KeyNegotiationCache redisKeyNegotiationCache(RedisTemplate<String, Object> redisTemplate,
                    @Value("${spring.application.name:nullApplicationName}") String applicationName){

        RedisKeyNegotiationCache keyNegotiationCache = new RedisKeyNegotiationCache(redisTemplate, applicationName);
        log.info("KeyNegotiationCache-redis init.");
        return keyNegotiationCache;
    }

    @Bean("localKeyNegotiationCache")
    @ConditionalOnMissingBean(value = {KeyNegotiationCache.class, RedisKeyNegotiationCache.class})
    public KeyNegotiationCache localKeyNegotiationCache(){
        return new LocalKeyNegotiationCache();
    }

    @Bean
    @ConditionalOnMissingBean(value = {RestTemplate.class})
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }

}
