package com.msgc.utils.crypto.asymmetric.dto;

import com.msgc.utils.crypto.common.ByteSpecification;
import lombok.Data;

import java.security.KeyPair;

/**
 * 非对称加密的 密钥对，主要用于外部（redis）存储
 * @author liuyanming 2019年12月18日
 */
@Data
public class KeyPairDto {
	/** 公钥 */
	private String pk;

	/** 私钥 */
	private String vk;

	KeyPairDto(){}
	public KeyPairDto(KeyPair keyPair){
		pk = ByteSpecification.encodeToString(keyPair.getPublic().getEncoded());
		vk = ByteSpecification.encodeToString(keyPair.getPrivate().getEncoded());
	}


}
