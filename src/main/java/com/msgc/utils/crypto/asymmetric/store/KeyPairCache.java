package com.msgc.utils.crypto.asymmetric.store;

import com.msgc.utils.crypto.asymmetric.exception.NoSuchKeyPairException;

import java.security.KeyPair;

/**
 * 密钥对存储
 * 		如果分布式部署，需要共享存储
 *
 * @author liuyanming 2019年12月18日
 */
public interface KeyPairCache {
	/**
	 * 存储密钥对
	 * @param id		id
	 * @param keyPair	密钥对
	 */
	void set(String id, KeyPair keyPair);
	
	/**
	 * 获取密钥对
	 * @param id 	id
	 * @return 		密钥对
	 * @throws NoSuchKeyPairException 密钥对缺失
	 */
	KeyPair get(String id) throws NoSuchKeyPairException;
	
	/**
	 * 销毁
	 */
	void destroy();
}
