package com.msgc.notify;

import com.msgc.config.LoggedUserSessionContext;
import com.msgc.constant.SessionKey;
import com.msgc.entity.Message;
import com.msgc.service.IMessageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpSession;

/**
 * @author Admin
 */
@Slf4j
@Component
public class MessageSender implements Runnable {

    private volatile boolean isRunning = true;

    private final IMessageService messageService;

    @Autowired
    public MessageSender(IMessageService messageService) {
        this.messageService = messageService;
    }

    public void stop(){
        isRunning = false;
    }

    @Override
    public void run() {
        if(messageService == null){
            log.error("MessageSender start fail !!");
            return;
        }
        log.info("MessageSender start success.");
        while (isRunning){
            try{
                Message message = MessageQueue.take();
                //消费 Message
                Integer userId = message.getReceiver();
                HttpSession session = LoggedUserSessionContext.getSession(userId);
                if(session != null){
                    session.setAttribute(SessionKey.NEW_MESSAGES_FLAG,true);
                }
                messageService.save(message);
            }catch(Exception e){
                e.printStackTrace();
            }
        }
        log.info("MessageSender stop success.");
    }
}
