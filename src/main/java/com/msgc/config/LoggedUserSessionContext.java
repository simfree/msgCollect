package com.msgc.config;

import javax.servlet.http.HttpSession;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class LoggedUserSessionContext{

    /**  根据自己网站的访问量设置一个合理的初始值 todo 应该改成列表 Map<Integer, List<HttpSession>> 表示一个用户可以在多个地方登录 */
    private static volatile Map<Integer, HttpSession> sessionMap  = new ConcurrentHashMap<>(128);

    public static HttpSession putIfAbsent(Integer userId, HttpSession session) {
    	//先前已经在某个客户端登录了，则返回那个 session，以实现多端登录session共享
	    return sessionMap.putIfAbsent(userId, session);        
    }

    public static void remove(Integer userId) {
        sessionMap.remove(userId);
    }
 
    public static HttpSession getSession(Integer userId) {
        return sessionMap.get(userId);
    }

    public static int size(){
        return sessionMap.size();
    }
}
