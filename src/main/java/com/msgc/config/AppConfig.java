package com.msgc.config;

import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.nio.file.Paths;

/**
 * 应用配置
 * @author liuyanming 2019年10月24日14:34:27
 */
@Setter
@Component
@ConfigurationProperties(prefix = "msgc")
public class AppConfig {

    /**
     * aesInfoFilePath
     */
    @Value("${aesPath:''}")
    String aesPath;


    /**
     * 判断是否为开发模式.
     * 开发模式开启方式: 启动参数添加 -DevMode=true
     */
    public static boolean devMode(){
        return "true".equals(System.getProperty("evMode"));
    }

    private boolean pathExist(String path){
        return StringUtils.isNotBlank(path) && Paths.get(path).toFile().exists();
    }

}
