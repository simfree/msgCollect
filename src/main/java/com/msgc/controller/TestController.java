package com.msgc.controller;

import com.msgc.utils.JsonUtil;
import com.msgc.utils.crypto.negotiation.cache.dto.KeyExchangeResult;
import com.msgc.utils.crypto.negotiation.constant.KeyExchangeConstants;
import com.msgc.utils.crypto.negotiation.dto.KeyExchangeRequest;
import com.msgc.utils.crypto.negotiation.dto.KeyExchangeResponse;
import com.msgc.utils.crypto.negotiation.exception.NegotiationException;
import com.msgc.utils.crypto.negotiation.service.TransportNegotiationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Description: 验证一些东西
 * @author LYM
 */
@RestController
public class TestController {

    @Autowired
    TransportNegotiationService transportNegotiationService;

    @GetMapping("/testEcdh")
    public String ecdhRequest() throws NegotiationException {
        KeyExchangeResult result = transportNegotiationService.requestForNegotiate("http://127.0.0.1:8888" + KeyExchangeConstants.DEFAULT_NEGOTIATION_URL);
        return JsonUtil.toJson(result);
    }

    @PostMapping(KeyExchangeConstants.DEFAULT_NEGOTIATION_URL)
    public KeyExchangeResponse ecdhRequest(@RequestBody KeyExchangeRequest keyExchangeRequest) throws NegotiationException {
        KeyExchangeResponse response = transportNegotiationService.handleNegotiate(keyExchangeRequest);
        JsonUtil.toJson(response);
        return response;
    }
}
