package com.msgc.listener;

import com.msgc.notify.MessageSender;
import com.msgc.utils.crypto.aes.AesCryptoAble;
import com.msgc.utils.crypto.asymmetric.AsymmetricCryptoService;
import com.msgc.utils.crypto.asymmetric.exception.AsymmetricCryptoException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.ContextRefreshedEvent;

/**
 * 项目启动
 * @author liuyanming 2019年10月29日
 */
@Slf4j
@Configuration
public class AppContextRefreshedListener implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    private AesCryptoAble aes;

    @Autowired
    private MessageSender msgSender;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        if(event.getApplicationContext().getParent() == null) {
            // 开启消息队列
            startHandleMsg();
            // 初始化 aes
            aes.ensureEncryption();
        }
    }

    private void startHandleMsg(){
        new Thread(msgSender, "notify_Thread").start();
        log.info("send order to messageSender start!");
    }

}
